from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config('DEBUG', cast=bool)

ALLOWED_HOSTS = [
    config('LOCALHOST'), 
    config('LOCAL_IP'), 
    'server_ip_address', 
    'www.domain.com'
]

# Application definition


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE'    : 'django.db.backends.postgresql_psycopg2',
        'NAME'      : config('DB_NAME'),
        'USER'      : config('DB_USERNAME'),
        'PASSWORD'  : config('DB_PASSWORD'),
        'HOST'      : config('DB_HOST'),
        'PORT'      : ''
    }
}