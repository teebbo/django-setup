Django==2.2.12
django-debug-toolbar==2.2
python-decouple==3.3
pytz==2020.1
sqlparse==0.3.1
