# django-setup

Django rapid setup for development.

# Getting started

Follow the steps describe below:

1. Clone/download this repository
2. Navigate to `src` folder and configure your `.env` file
3. Add `.env` file to your `.gitignore` file
4. Create a virtual environnement and install dependencies with `pip install -r requirements.txt`
5. Rename the project with `python manage.py rename <actual-project-name> <new-project-name>`